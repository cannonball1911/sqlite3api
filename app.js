const express = require('express')
const morgan = require('morgan')
const bodyParser = require('body-parser')

const {initDB} = require('./database/initDB')

let db = null
const app = express()
const router = express.Router()

main()

router.get('/rezepte', (req, res) => {
    console.log('Fetch rezepte')
    const sql = 'SELECT * FROM rezepte'
    db.all(sql, (err, rows) => {
        if (err) {
            console.log(err)
            return res.sendStatus(500).end()
        }

        const rezepte = []
        rows.forEach((row) => {
            let rezept = { rezepteID: row.rezepteID, rezept: row.rezept, zubereitungsdauer: row.zubereitungsdauer }
            rezepte.push(rezept)
        })
        res.json({
            "links": {
                "self": req.originalUrl
            },
            "data": rezepte
        })
    })
})

router.get('/zutaten', (req, res) => {
    console.log('Fetch zutaten')
    const sql = 'SELECT * FROM zutaten'
    db.all(sql, (err, rows) => {
        if (err) {
            console.log(err)
            return res.sendStatus(500).end()
        }

        const zutaten = []
        rows.forEach((row) => {
            let zutat = { zutatenID: row.zutatenID, zutat: row.zutat }
            zutaten.push(zutat)
        })
        res.json({
            "links": {
                "self": req.originalUrl
            },
            "data": zutaten
        })
    })
})

router.get('/rezepteZutaten', (req, res) => {
    console.log('Fetch rezepteZutaten')
    const sql = 'SELECT * FROM rezepte_x_zutaten AS rz INNER JOIN rezepte AS r ON r.rezepteID = rz.rezepteID INNER JOIN zutaten AS z ON z.zutatenID = rz.zutatenID'
    db.all(sql, (err, rows) => {
        if (err) {
            console.log(err)
            return res.sendStatus(500).end()
        }

        const rezepteZutaten = []
        rows.forEach((row) => {
            let rezeptZutaten = {
                rezepteID: row.rezepteID,
                zutatenID: row.zutatenID,
                rezept: row.rezept,
                zubereitungsdauer: row.zubereitungsdauer,
                zutat: row.zutat
            }
            rezepteZutaten.push(rezeptZutaten)
        })
        res.json({
            "links": {
                "self": req.originalUrl
            },
            "data": rezepteZutaten
        })
    })
})

router.get('/rezepteZutaten/:id', (req, res) => {
    const id = req.params.id
    console.log(`Fetch all rezepte with ID ${id}`)
    const sql = 'SELECT * FROM rezepte_x_zutaten AS rz INNER JOIN rezepte AS r ON r.rezepteID = rz.rezepteID INNER JOIN zutaten AS z ON z.zutatenID = rz.zutatenID WHERE rz.rezepteID = $id'
    db.all(sql, [id], (err, rows) => {
        if (err) {
            console.log(err)
            return res.sendStatus(500).end()
        }

        const rezepteZutaten = []
        rows.forEach((row) => {
            let rezeptZutaten = {
                rezepteID: row.rezepteID,
                zutatenID: row.zutatenID,
                rezept: row.rezept,
                zubereitungsdauer: row.zubereitungsdauer,
                zutat: row.zutat
            }
            rezepteZutaten.push(rezeptZutaten)
        })
        res.json({
            "links": {
                "self": req.originalUrl
            },
            "data": rezepteZutaten
        })
    })
})

router.post('/rezepte', (req, res) => {
    console.log('Create new rezept')
    const rezept = req.body.rezept
    const dauer = req.body.zubereitungsdauer
    if (!rezept || !dauer) {
        return res.sendStatus(400).end()
    }

    const sql = 'INSERT INTO rezepte (rezept, zubereitungsdauer) VALUES ($rezept, $dauer)'
    db.run(sql, [rezept, dauer], function (err) {
        if (err) {
            console.log(err)
            return res.sendStatus(500).end()
        }

        res.json({
            "links": {
                "self": req.originalUrl
            },
            "data": [{
                rezepteID: this.lastID,
                rezept,
                zubereitungsdauer: dauer
            }]
        })
    })
})

router.post('/zutaten', (req, res) => {
    console.log('Create new zutat')
    const zutat = req.body.zutat
    if (!zutat) {
        return res.sendStatus(400).end()
    }

    const sql = 'INSERT INTO zutaten (zutat) VALUES ($zutat)'
    db.run(sql, [zutat], function (err) {
        if (err) {
            console.log(err)
            return res.sendStatus(500).end()
        }

        res.json({
            "links": {
                "self": req.originalUrl
            },
            "data": [{
                zutatenID: this.lastID,
                zutat
            }]
        })
    })
})

router.delete('/rezepte/:id', (req, res) => {
    const id = req.params.id
    console.log(`Delete rezept with ID ${id}`)
    const sqlGet = 'SELECT * FROM rezepte WHERE rezepteID = $id'
    const sqlDel = 'DELETE FROM rezepte WHERE rezepteID = $id'
    db.get(sqlGet, [id], (err, row) => {
        if (err) {
            console.log(err)
            return res.sendStatus(500).end()
        } else if (!row) {
            return res.sendStatus(404).end()
        }

        const delData = [{
            rezepteID: id,
            rezept: row.rezept,
            zubereitungsdauer: row.zubereitungsdauer
        }]

        db.run(sqlDel, [id], function (err) {
            if (err) {
                console.log(err)
                return res.sendStatus(500).end()
            }

            res.json({
                "links": {
                    "self": req.originalUrl
                },
                "data": delData
            })
        })
    })
})

router.delete('/zutaten/:id', (req, res) => {
    const id = req.params.id
    console.log(`Delete zutat with ID ${id}`)
    const sqlGet = 'SELECT * FROM zutaten WHERE zutatenID = $id'
    const sqlDel = 'DELETE FROM zutaten WHERE zutatenID = $id'

    db.get(sqlGet, [id], (err, row) => {
        if (err) {
            console.log(err)
            return res.sendStatus(500).end()
        } else if (!row) {
            return res.sendStatus(404).end()
        }

        const delData = [{
            zutatenID: id,
            zutat: row.zutat
        }]

        db.run(sqlDel, [id], function (err) {
            if (err) {
                console.log(err)
                return res.sendStatus(500).end()
            }

            res.json({
                "links": {
                    "self": req.originalUrl
                },
                "data": delData
            })
        })
    })
})

router.put('/rezepte/:id', (req, res) => {
    const id = req.params.id
    console.log(`Update rezept with ID ${id}`)
    const rezept = req.body.rezept
    const zubereitungsdauer = req.body.zubereitungsdauer
    const sqlGet = 'SELECT * FROM rezepte WHERE rezepteID = $id'
    const sqlPut = 'UPDATE rezepte SET rezept = $rezept, zubereitungsdauer = $zubereitungsdauer WHERE rezepteID = $id'

    db.get(sqlGet, [id], (err, row) => {
        if (err) {
            console.log(err)
            return res.sendStatus(500).end()
        } else if (!row) {
            return res.sendStatus(404).end()
        } else if (!rezept || !zubereitungsdauer) {
            return res.sendStatus(400).end()
        }

        db.run(sqlPut, [rezept, zubereitungsdauer, id], (err) => {
            if (err) {
                console.log(err)
                return res.sendStatus(500).end()
            }

            res.json({
                "links": {
                    "self": req.originalUrl
                },
                "data": [{
                    rezepteID: id,
                    rezept,
                    zubereitungsdauer
                }]
            })
        })
    })
})

router.put('/zutaten/:id', (req, res) => {
    const id = req.params.id
    console.log(`Update rezept with ID ${id}`)
    const zutat = req.body.zutat
    const sqlGet = 'SELECT * FROM zutaten WHERE zutatenID = $id'
    const sqlPut = 'UPDATE zutaten SET zutat = $zutat WHERE zutatenID = $id'

    db.get(sqlGet, [id], (err, row) => {
        if (err) {
            console.log(err)
            return res.sendStatus(500).end()
        } else if (!row) {
            return res.sendStatus(404).end()
        } else if (!zutat) {
            return res.sendStatus(400).end()
        }

        db.run(sqlPut, [zutat, id], (err) => {
            if (err) {
                console.log(err)
                return res.sendStatus(500).end()
            }

            res.json({
                "links": {
                    "self": req.originalUrl
                },
                "data": [{
                    zutatenID: id,
                    zutat
                }]
            })
        })
    })
})

function main() {
    db = initDB()
    app.use(bodyParser.json())
    app.use(morgan('short'))
    app.use('/api', router)
    app.listen(3000, () => {
        console.log('Server up on port 3000...')
    })
}