const fs = require('fs')
const sqlite3 = require('sqlite3').verbose()

const initDB = () => {
    let db = null
    console.log('Init Database...')
    if (fs.existsSync('./database/db.sqlite')) {
        db = new sqlite3.Database('./database/db.sqlite')
        console.log('DB up and running...')
    } else {
        db = new sqlite3.Database('./database/db.sqlite')
        db.serialize(() => {
            db.run('CREATE TABLE IF NOT EXISTS `rezepte` (rezepteID INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE, rezept TEXT NOT NULL, zubereitungsdauer INTEGER NOT NULL)')
            db.run('CREATE TABLE IF NOT EXISTS `zutaten` (zutatenID INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE, zutat TEXT NOT NULL)')
            db.run('CREATE TABLE IF NOT EXISTS `rezepte_x_zutaten` (rezepteID INTEGER REFERENCES rezepte (rezepteID) NOT NULL, zutatenID INTEGER REFERENCES zutaten (zutatenID) NOT NULL)')
            db.run('INSERT INTO `rezepte` (rezept, zubereitungsdauer) VALUES ("Kuchen", 20)')
            db.run('INSERT INTO `rezepte` (rezept, zubereitungsdauer) VALUES ("Torte", 35)')
            db.run('INSERT INTO `zutaten` (zutat) VALUES ("Eier")')
            db.run('INSERT INTO `zutaten` (zutat) VALUES ("Mehl")')
            db.run('INSERT INTO `zutaten` (zutat) VALUES ("Schokolade")')
            db.run('INSERT INTO `zutaten` (zutat) VALUES ("Milch")')
            db.run('INSERT INTO `zutaten` (zutat) VALUES ("Butter")')
            db.run('INSERT INTO `rezepte_x_zutaten` (rezepteID, zutatenID) VALUES (1, 1)')
            db.run('INSERT INTO `rezepte_x_zutaten` (rezepteID, zutatenID) VALUES (1, 3)')
            db.run('INSERT INTO `rezepte_x_zutaten` (rezepteID, zutatenID) VALUES (1, 2)')
            db.run('INSERT INTO `rezepte_x_zutaten` (rezepteID, zutatenID) VALUES (2, 5)')
            db.run('INSERT INTO `rezepte_x_zutaten` (rezepteID, zutatenID) VALUES (2, 2)')
        })
        console.log('DB created...')
    }
    return db
}

module.exports = {initDB}